#3.0.1
* 修复兼容2版本的PageQuery对象
* Fetch框架增强，可以自定义Fetch实现，如新增@FetchSql
* @FetchSql注解，用于加载额外对象，比@FetchOne和@FetchMany更灵活
* ClassAnnotation 一个问题修复
* Fetch框架只能是用一个Fetch注解的Bug修复

# 3.0.3
* jfinal 支持
# 3.0.4
* 翻页bug修复
* upsertByTemplate 返回false



